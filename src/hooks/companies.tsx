import {
  createContext,
  useContext,
  useCallback,
  useState,
  ReactNode,
} from 'react';

import { api } from '../services/axios';

interface IContextProps {
  isWaiting: boolean;
  setIsWaiting: (value: boolean) => void;
}

type MyGroupType = {
  [key: string]: string;
};

interface CompaniesProviderProps {
  children: ReactNode;
}

export const companiesHookContext = createContext({} as IContextProps);

export const CompaniesProvider = ({ children }: CompaniesProviderProps) => {
  const [isWaiting, setIsWaiting] = useState<boolean>(false);
  const [allCompanies, setAllCompanies] = useState<MyGroupType[]>([]);
  const [companyDetails, setCompanyDetails] = useState();
  const [getCompanyByName, setGetCompanyByName] = useState();

  const getAllCompanies = useCallback(async () => {
    setIsWaiting(true);
    const response = await api.get(`enterprises`);
    const result = response.data;
    // setAllCompanies(result);
    console.log(result);
    setIsWaiting(false);
  }, []);

  const showCompanyDetails = useCallback(async (companyId) => {
    setIsWaiting(true);
    const response = await api.get(`enterprises/${companyId}`);
    const result = response.data;
    // setCompanyDetails(result);
    console.log(result);
    setIsWaiting(false);
  }, []);

  const showCompanyByName = useCallback(async (companyName) => {
    setIsWaiting(true);
    const response = await api.get(`enterprises/${companyName}`);
    const result = response.data;
    // setGetCompanyByName(result);
    console.log(result);
    setIsWaiting(false);
  }, []);

  return (
    <companiesHookContext.Provider
      value={{
        allCompanies,
        getAllCompanies,
        showCompanyDetails,
        companyDetails,
        showCompanyByName,
        getCompanyByName,
        isWaiting,
      }}
    >
      {children}
    </companiesHookContext.Provider>
  );
};

export function useCompaniesHooks() {
  const context = useContext(companiesHookContext);

  if (!context) {
    throw new Error(
      'defaultHooksContext must be use within an CompaniesProvider',
    );
  }

  return context;
}
