import { Stack, Typography } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import colors from '../../colors/colors';
import CompanyCard from '../../components/CompanyCard';
import CompanyCardDetails from '../../components/CompanyCardDetails';
import SearchComponent from '../../components/SearchComponent';
import { api } from '../../services/axios';

const Home = () => {
  const { push } = useHistory();
  const [idCardDetails, setIdCardDetails] = useState(0);
  const [showAllCompanies, setShowAllCompanies] = useState<any>({});
  const [openSearchBar, setOpenSearchBar] = useState(false);
  const [openDetails, setOpenDetails] = useState(false);
  const { headers } = JSON.parse(sessionStorage.getItem('userData') || '{}');
  const handleCardClick = (companyId: number) => {
    setOpenDetails(true);
    setIdCardDetails(idCardDetails);
    push(`/company-details/${companyId}`);
  };
  // const showCompanyByIndexHandler = async () => {
  //   const res = await api.get(
  //     `https://empresas.ioasys.com.br/api/v1/enterprises/${idCardDetails}`,
  //     {
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'access-token': headers['access-token'],
  //         client: headers.client,
  //         uid: headers.uid,
  //         Authorization: `Bearer ${headers['access-token']}`,
  //       },
  //     },
  //   );
  //   setCompaniesByIndex(res.data);
  // };
  const showAllCompaniesHandler = async () => {
    const res = await api.get(
      `https://empresas.ioasys.com.br/api/v1/enterprises`,
      {
        headers: {
          'Content-Type': 'application/json',
          'access-token': headers['access-token'],
          client: headers.client,
          uid: headers.uid,
          Authorization: `Bearer ${headers['access-token']}`,
        },
      },
    );
    setShowAllCompanies(res.data);
  };
  useEffect(() => {
    showAllCompaniesHandler();
  }, []);
  return (
    <Stack
      sx={{
        alignItems: 'center',
        bgcolor: colors.loginBackground,
        bottom: 0,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0,
      }}
    >
      <SearchComponent
        openSearchBar={openSearchBar}
        setOpenSearchBar={setOpenSearchBar}
        openDetails={openDetails}
        setOpenDetails={setOpenDetails}
      >
        {openSearchBar && !openDetails && (
          <Stack
            sx={{ mt: '44px', overflowY: 'hidden', px: '50px', width: '100%' }}
          >
            <Stack
              sx={{
                overflowY: 'scroll',
                '::-webkit-scrollbar': { display: 'none' },
              }}
            >
              {showAllCompanies.enterprises.map((company: any) => (
                <CompanyCard
                  title={company.enterprise_name}
                  enterprise_type={company.enterprise_type.enterprise_type_name}
                  country={company.country}
                  photo={`https://empresas.ioasys.com.br${company.photo}`}
                  onCardClick={() => handleCardClick(company.id)}
                />
              ))}
            </Stack>
          </Stack>
        )}
        {openDetails && (
          <>
            <Stack sx={{ overflowY: 'hidden' }}>
              <Stack
                sx={{
                  overflowY: 'scroll',
                  '::-webkit-scrollbar': { display: 'none' },
                }}
              >
                <CompanyCardDetails description="sss" photo="sssss" />
              </Stack>
            </Stack>
          </>
        )}
      </SearchComponent>
      {!openSearchBar && !openDetails && (
        <Typography
          sx={{ color: colors.charcoalGrey, fontSize: 32, mt: '360px' }}
        >
          Clique na busca para iniciar.
        </Typography>
      )}
    </Stack>
  );
};

export default Home;
