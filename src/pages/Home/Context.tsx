import { createContext } from 'react';

type ContextType = {
  term: string;
  setTerm: (term: string) => void;
};

const Context = createContext<ContextType>({} as ContextType);

export default Context;
