/* eslint-disable no-nested-ternary */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Box, Button, Input, Stack, Typography } from '@material-ui/core';
import {
  Error as ErrorIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from '@material-ui/icons';
import { useFormik } from 'formik';
import { StatusCodes } from 'http-status-codes';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';

import lockerIcon from '../../assets/png/ic-cadeado@3x.png';
import emailIcon from '../../assets/png/ic-email@3x.png';
import logoIoasys from '../../assets/png/logo-home@3x.png';
import colors from '../../colors/colors';
import { api } from '../../services/axios';
import { submitButton } from './styles';

interface LoginInput {
  email: string;
  password: string;
}

const Login = () => {
  const { push } = useHistory();
  const [invalidCredentials, setInvalidCredentials] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const showPasswordHandler = () => {
    setShowPassword(!showPassword);
  };
  const login = (input: LoginInput) =>
    api.post('/v1/users/auth/sign_in', input);
  const form = useFormik<LoginInput>({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: yup.object().shape({
      email: yup.string().required().email(),
      password: yup.string().required(),
    }),
    onSubmit: async (input) => {
      try {
        const res = await login(input);
        sessionStorage.setItem('userData', JSON.stringify(res));
        push('/home');
      } catch (err: any) {
        const containsInvalidCredentials =
          err.response?.status === StatusCodes.UNAUTHORIZED;
        if (containsInvalidCredentials) {
          setInvalidCredentials(true);
        }
      }
    },
  });
  return (
    <Stack
      sx={{
        alignItems: 'center',
        bgcolor: colors.loginBackground,
        bottom: 0,
        justifyContent: 'center',
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0,
      }}
    >
      <Stack
        sx={{
          alignItems: 'center',
          width: 348,
        }}
      >
        <Box
          sx={{
            backgroundImage: `url(${logoIoasys})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'contain',
            height: 72,
            width: 295,
          }}
        />
        <Typography
          sx={{
            color: colors.charcoalGrey,
            fontSize: 23,
            fontWeight: 'bold',
            mb: '25px',
            mt: '65px',
            textAlign: 'center',
            width: 172,
          }}
        >
          BEM-VINDO AO EMPRESAS
        </Typography>
        <Typography
          sx={{
            color: colors.charcoalGrey,
            fontSize: 17,
            mb: '46px',
            textAlign: 'center',
          }}
        >
          Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </Typography>
        <form onSubmit={form.handleSubmit}>
          <Input
            value={form.values.email}
            name="email"
            error={!!form.errors.email}
            startAdornment={
              <img
                src={emailIcon}
                alt="locker"
                style={{ height: '27px', marginRight: '10px', width: '27px' }}
              />
            }
            endAdornment={
              invalidCredentials ? (
                <ErrorIcon sx={{ color: colors.error }} />
              ) : (
                ''
              )
            }
            sx={{ mb: '33px', fontSize: 18, width: '100%' }}
            placeholder="E-mail"
            onChange={form.handleChange}
          />
          <Input
            type={showPassword ? 'text' : 'password'}
            value={form.values.password}
            name="password"
            error={!!form.errors.password}
            startAdornment={
              <img
                src={lockerIcon}
                alt="locker"
                style={{
                  height: '27px',
                  marginRight: '10px',
                  width: '27px',
                }}
              />
            }
            endAdornment={
              invalidCredentials ? (
                <ErrorIcon sx={{ color: colors.error }} />
              ) : showPassword ? (
                <VisibilityOffIcon
                  sx={{
                    color: 'rgba(0, 0, 0, 0.54)',
                    cursor: 'pointer',
                    height: 20,
                    width: 29,
                  }}
                  onClick={showPasswordHandler}
                />
              ) : (
                <VisibilityIcon
                  sx={{
                    color: 'rgba(0, 0, 0, 0.54)',
                    cursor: 'pointer',
                    height: 20,
                    width: 29,
                  }}
                  onClick={showPasswordHandler}
                />
              )
            }
            placeholder="Senha"
            sx={{
              bgcolor: colors.loginBackground,
              fontSize: 18,
              width: '100%',
            }}
            onChange={form.handleChange}
          />
        </form>
        {invalidCredentials && (
          <Typography sx={{ color: colors.error, fontSize: 12, mt: '13px' }}>
            Credenciais informadas são inválidas, tente novamente.
          </Typography>
        )}
        <Button
          variant="contained"
          sx={submitButton}
          onClick={() => form.handleSubmit()}
        >
          Entrar
        </Button>
      </Stack>
    </Stack>
  );
};

export default Login;
