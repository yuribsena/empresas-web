import colors from '../../colors/colors';

export const submitButton = {
  bgcolor: colors.greenyBlue,
  fontSize: 18,
  height: 52,
  mt: '46px',
  width: '100%',
};
