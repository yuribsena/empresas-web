import { Typography } from '@material-ui/core';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import ViewContainer from '../../components/ViewContainer';
import { api } from '../../services/axios';

interface ParamsProps {
  id: string;
}

/// headers deve vir do contexto de login do usuario
const CompanyDetails = () => {
  const CompanyName = () => <Typography>Empresa</Typography>;
  const { id } = useParams<ParamsProps>();
  const showCompanyByIndexHandler = async () => {
    const res = await api.get(
      `https://empresas.ioasys.com.br/api/v1/enterprises/${id}`,
      {
        headers: {
          'Content-Type': 'application/json',
          'access-token': headers['access-token'],
          client: headers.client,
          uid: headers.uid,
          Authorization: `Bearer ${headers['access-token']}`,
        },
      },
    );
    console.log(res);
  };
  useEffect(() => {
    showCompanyByIndexHandler();
  }, []);
  return (
    <ViewContainer headerChildren={<CompanyName />}>
      <Typography>detalhes da empresa</Typography>
    </ViewContainer>
  );
};

export default CompanyDetails;
