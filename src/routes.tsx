import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import CompanyDetails from './pages/CompanyDetails';
import Home from './pages/Home';
import Login from './pages/Login/Login';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route
        exact
        path="/"
        render={() => {
          return <Redirect to="/login" />;
        }}
      />
      <Route exact path="/login">
        <Login />
      </Route>
      <Route exact path="/home">
        <Home />
      </Route>
      <Route exact path="/company-details/:id">
        <CompanyDetails />
      </Route>
    </Switch>
  </BrowserRouter>
);

export default Routes;
