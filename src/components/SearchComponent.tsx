/* eslint-disable react/require-default-props */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Box, Input, Stack, Typography } from '@material-ui/core';
import {
  ArrowBack as ArrowBackIcon,
  Close as CloseIcon,
  Search as SearchIcon,
} from '@material-ui/icons';
import { ReactNode, useState } from 'react';

import logoIoasys from '../assets/png/logo-nav@3x.png';
import colors from '../colors/colors';

interface SearchComponentProps {
  openSearchBar: boolean;
  setOpenSearchBar: (arg: boolean) => void;
  openDetails: boolean;
  setOpenDetails: (arg: boolean) => void;
  children?: ReactNode;
}

const SearchComponent = ({
  openSearchBar,
  setOpenSearchBar,
  openDetails,
  setOpenDetails,
  children,
}: SearchComponentProps) => {
  const [term, setTerm] = useState('');
  return (
    <>
      <Stack
        sx={{
          bgcolor: colors.headerBackground,
          boxSizing: 'border-box',
          flexDirection: 'row',
          minHeight: 150,
          width: '100%',
        }}
      >
        {!openSearchBar && !openDetails && (
          <>
            <Stack
              sx={{ flex: 2, alignItems: 'flex-end', justifyContent: 'center' }}
            >
              <Box
                sx={{
                  backgroundImage: `url(${logoIoasys})`,
                  backgroundSize: 'contain',
                  height: 57,
                  width: 234,
                }}
              />
            </Stack>
            <Stack
              sx={{
                alignItems: 'center',
                flex: 1.5,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                mr: '40px',
              }}
            >
              <SearchIcon
                sx={{
                  color: colors.white,
                  cursor: 'pointer',
                  height: 50,
                  width: 50,
                }}
                onClick={() => setOpenSearchBar(true)}
              />
            </Stack>
          </>
        )}
        {openSearchBar && !openDetails && (
          <Stack
            sx={{
              justifyContent: 'flex-end',
              mb: '24px',
              mx: '42px',
              width: '100%',
            }}
          >
            <Input
              placeholder="Pesquisar"
              value={term}
              startAdornment={
                <SearchIcon
                  sx={{
                    color: colors.white,
                    cursor: 'pointer',
                    height: 50,
                    width: 50,
                  }}
                />
              }
              endAdornment={
                <CloseIcon
                  sx={{ cursor: 'pointer', height: 30, width: 30 }}
                  onClick={() => setOpenSearchBar(false)}
                />
              }
              sx={{ color: colors.white, fontSize: 34, width: '100%' }}
              onChange={(evt) => setTerm(evt.target.value)}
              onKeyPress={(evt) => {
                if (evt.key === 'Enter') {
                  evt.preventDefault();
                }
              }}
            />
          </Stack>
        )}
        {openDetails && (
          <Stack
            sx={{
              alignItems: 'flex-end',
              flexDirection: 'row',
              pb: '34px',
              px: '42px',
            }}
          >
            <ArrowBackIcon
              sx={{
                color: colors.white,
                cursor: 'pointer',
                height: 50,
                mr: '68px',
                width: 50,
              }}
              onClick={() => {
                setOpenDetails(false);
                setOpenSearchBar(true);
              }}
            />
            <Typography sx={{ color: colors.white, fontSize: 34 }}>
              EMPRESA1
            </Typography>
          </Stack>
        )}
      </Stack>
      {children}
    </>
  );
};

export default SearchComponent;
