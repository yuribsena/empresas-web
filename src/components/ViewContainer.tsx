import { Stack } from '@material-ui/core';
import { ReactNode } from 'react';

import colors from '../colors/colors';

interface SearchComponentProps {
  headerChildren: ReactNode;
  children: ReactNode;
}

const ViewContainer = ({ headerChildren, children }: SearchComponentProps) => {
  return (
    <>
      <Stack
        sx={{
          bgcolor: colors.headerBackground,
          boxSizing: 'border-box',
          flexDirection: 'row',
          minHeight: 150,
          width: '100%',
        }}
      >
        {headerChildren}
      </Stack>
      {children}
    </>
  );
};

export default ViewContainer;
