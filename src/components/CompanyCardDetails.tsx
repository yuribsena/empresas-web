import { Box, Stack, Typography } from '@material-ui/core';

import colors from '../colors/colors';

interface CompanyCardDetailsProps {
  description: string;
  photo: string;
}

const CompanyCardDetails = ({
  description,
  photo,
}: CompanyCardDetailsProps) => {
  return (
    <Stack
      sx={{
        bgcolor: colors.white,
        mx: '50px',
        my: '44px',
        pb: '195px',
        pt: '48px',
        px: '73px',
      }}
    >
      <Box
        sx={{
          backgroundImage: photo,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'contain',
          height: 295,
          width: '100%',
        }}
      />
      <Typography sx={{ color: colors.warmGrey, fontSize: 34, mt: '49px' }}>
        {description}
      </Typography>
    </Stack>
  );
};

export default CompanyCardDetails;
