/* eslint-disable react/no-unused-prop-types */
import { Box, Stack, Typography } from '@material-ui/core';

import colors from '../colors/colors';

interface CompanyCardProps {
  title: string;
  enterprise_type: string;
  country: string;
  photo: string;
  onCardClick: () => void;
}

const CompanyCard = ({
  title,
  enterprise_type,
  country,
  photo,
  onCardClick,
}: CompanyCardProps) => {
  return (
    <Stack
      sx={{
        alignItems: 'center',
        bgcolor: colors.white,
        cursor: 'pointer',
        flexDirection: 'row',
        minHeight: 218,
        mb: '30px',
        px: '30px',
        width: '100%',
      }}
      onClick={onCardClick}
    >
      <Box
        sx={{
          backgroundImage: `url(${photo})`,
          backgroundSize: 'contain',
          height: 160,
          width: 293,
        }}
      />
      <Stack sx={{ ml: '40px' }}>
        <Typography
          sx={{ color: colors.darkIndigo, fontSize: 30, fontWeight: 'bold' }}
        >
          {title}
        </Typography>
        <Typography sx={{ color: colors.warmGrey, fontSize: 24 }}>
          {enterprise_type}
        </Typography>
        <Typography sx={{ color: colors.warmGrey, fontSize: 18 }}>
          {country}
        </Typography>
      </Stack>
    </Stack>
  );
};

export default CompanyCard;
