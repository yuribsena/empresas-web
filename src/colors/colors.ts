const colors = {
  black: '#000',
  charcoalGrey: '#383743',
  darkIndigo: '#1a0e49',
  error: '#ff0f44',
  greenyBlue: '#57bbbc',
  headerBackground: '#ee4c77',
  loginBackground: '#eeecdb',
  warmGrey: '#8d8c8c',
  white: '#fff',
};

export default colors;
